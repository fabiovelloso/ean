/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ean13gen;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfiguration;
import org.krysalis.barcode4j.BarcodeException;
import org.krysalis.barcode4j.BarcodeGenerator;
import org.krysalis.barcode4j.BarcodeUtil;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.output.eps.EPSCanvasProvider;
import org.krysalis.barcode4j.output.svg.SVGCanvasProvider;

import org.krysalis.barcode4j.tools.MimeTypes;

/**
 *
 * @author fabio
 */
public class Test {

    private static final String BARCODE_TYPE = "ean13";
    private static final String BARCODE_FORMAT = "svg";
    private static final String BARCODE_HEIGHT = null;
    private static final String BARCODE_MODULE_WIDTH = null;
    private static final String BARCODE_WIDE_FACTOR = null;
    private static final String BARCODE_QUIET_ZONE = null;

    private static final String BARCODE_HUMAN_READABLE_POS = null;
    private static final String BARCODE_HUMAN_READABLE_PATTERN = null;
    private static final String BARCODE_HUMAN_READABLE_SIZE = null;
    private static final String BARCODE_HUMAN_READABLE_FONT = null;
    private static final String BARCODE_IMAGE_RESOLUTION = "220";
    private static final boolean BARCODE_IMAGE_GRAYSCALE = true;
     private static final String DIR = "images";

    private final RandomEAN randomEAN = new RandomEAN();

    public static void main(String[] args) {
        Test t = new Test();
        int num = 1;
        String dir = DIR;
        if (args.length > 1) {
            try {
                num = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an integer.");
                System.exit(1);
            }
            dir = args[1];
        }
        else {
             System.err.println("Arguments must be equals  2");
              System.exit(1);
        }
        

        t.exec(num,dir);
    }

    private void exec(int num, String dir) {
        try {
            generate(num,dir);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void generate(int numero,String dir) throws IOException {

        try {
            String format = determineFormat();
            int orientation = 0;
            Configuration cfg = buildCfg();
            BarcodeUtil util = BarcodeUtil.getInstance();
            BarcodeGenerator gen = util.createBarcodeGenerator(cfg);

            ByteArrayOutputStream bout;
            String msg;
            for (int i = 0; i < numero; i++) {
                msg = Long.toString(randomEAN.generateRandom());
                bout = new ByteArrayOutputStream(4096);
                try {
                    if (format.equals(MimeTypes.MIME_SVG)) {
                        //Create Barcode and render it to SVG
                        SVGCanvasProvider svg = new SVGCanvasProvider(false, orientation);
                        gen.generateBarcode(svg, msg);
                        org.w3c.dom.DocumentFragment frag = svg.getDOMFragment();

                        //Serialize SVG barcode
                        TransformerFactory factory = TransformerFactory.newInstance();
                        Transformer trans = factory.newTransformer();
                        Source src = new javax.xml.transform.dom.DOMSource(frag);
                        Result res = new javax.xml.transform.stream.StreamResult(bout);
                        trans.transform(src, res);
                    } else if (format.equals(MimeTypes.MIME_EPS)) {
                        EPSCanvasProvider eps = new EPSCanvasProvider(bout, orientation);
                        gen.generateBarcode(eps, msg);
                        eps.finish();
                    } else {
                        String resText = BARCODE_IMAGE_RESOLUTION;
                        int resolution = 300; //dpi
                        if (resText != null) {
                            resolution = Integer.parseInt(resText);
                        }
                        if (resolution > 2400) {
                            throw new IllegalArgumentException(
                                    "Resolutions above 2400dpi are not allowed");
                        }
                        if (resolution < 10) {
                            throw new IllegalArgumentException(
                                    "Minimum resolution must be 10dpi");
                        }
                    //    String gray = BARCODE_IMAGE_GRAYSCALE;
                     //   BitmapCanvasProvider bitmap = ("true".equalsIgnoreCase(gray)
                     BitmapCanvasProvider bitmap = (BARCODE_IMAGE_GRAYSCALE == true
                                ? new BitmapCanvasProvider(
                                        bout, format, resolution,
                                        BufferedImage.TYPE_BYTE_GRAY, true, orientation)
                                : new BitmapCanvasProvider(
                                        bout, format, resolution,
                                        BufferedImage.TYPE_BYTE_BINARY, false, orientation));
                        gen.generateBarcode(bitmap, msg);
                        bitmap.finish();
                    }
                } finally {
                    bout.close();

                }
                System.out.println(msg + ".svg");
                FileOutputStream file = new FileOutputStream(dir+"/"+msg + ".svg");
                file.write(bout.toByteArray());
                bout.close();
                bout = null;
                file.flush();
            }

        } catch (IOException | IllegalArgumentException | TransformerException | ConfigurationException | BarcodeException e) {
            System.out.println("Error while generating barcode");
            e.printStackTrace();
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, e);

        } catch (Throwable t) {
            System.out.println("Error while generating barcode");
            t.printStackTrace();

        }

    }

  

    protected String determineFormat() {
        String format = BARCODE_FORMAT;
        format = MimeTypes.expandFormat(format);
        if (format == null) {
            format = MimeTypes.MIME_SVG;
        }
        return format;
    }

    protected Configuration buildCfg() {
        DefaultConfiguration cfg = new DefaultConfiguration("barcode");
        //Get type
        String type = BARCODE_TYPE;
        if (type == null) {
            type = "code128";
        }
        DefaultConfiguration child = new DefaultConfiguration(type);
        cfg.addChild(child);
        //Get additional attributes
        DefaultConfiguration attr;
        // String height = request.getParameter(BARCODE_HEIGHT);
        String height = BARCODE_HEIGHT;
        if (height != null) {
            attr = new DefaultConfiguration("height");
            attr.setValue(height);
            child.addChild(attr);
        }
        String moduleWidth = BARCODE_MODULE_WIDTH;
        if (moduleWidth != null) {
            attr = new DefaultConfiguration("module-width");
            attr.setValue(moduleWidth);
            child.addChild(attr);
        }
        String wideFactor = BARCODE_WIDE_FACTOR;
        if (wideFactor != null) {
            attr = new DefaultConfiguration("wide-factor");
            attr.setValue(wideFactor);
            child.addChild(attr);
        }
        String quietZone = BARCODE_QUIET_ZONE;
        if (quietZone != null) {
            attr = new DefaultConfiguration("quiet-zone");
            if (quietZone.startsWith("disable")) {
                attr.setAttribute("enabled", "false");
            } else {
                attr.setValue(quietZone);
            }
            child.addChild(attr);
        }

        // creating human readable configuration according to the new Barcode Element Mappings
        // where the human-readable has children for font name, font size, placement and
        // custom pattern.
        String humanReadablePosition = BARCODE_HUMAN_READABLE_POS;
        String pattern = BARCODE_HUMAN_READABLE_PATTERN;
        String humanReadableSize = BARCODE_HUMAN_READABLE_SIZE;
        String humanReadableFont = BARCODE_HUMAN_READABLE_FONT;

        if (!((humanReadablePosition == null)
                && (pattern == null)
                && (humanReadableSize == null)
                && (humanReadableFont == null))) {
            attr = new DefaultConfiguration("human-readable");

            DefaultConfiguration subAttr;
            if (pattern != null) {
                subAttr = new DefaultConfiguration("pattern");
                subAttr.setValue(pattern);
                attr.addChild(subAttr);
            }
            if (humanReadableSize != null) {
                subAttr = new DefaultConfiguration("font-size");
                subAttr.setValue(humanReadableSize);
                attr.addChild(subAttr);
            }
            if (humanReadableFont != null) {
                subAttr = new DefaultConfiguration("font-name");
                subAttr.setValue(humanReadableFont);
                attr.addChild(subAttr);
            }
            if (humanReadablePosition != null) {
                subAttr = new DefaultConfiguration("placement");
                subAttr.setValue(humanReadablePosition);
                attr.addChild(subAttr);
            }

            child.addChild(attr);
        }
        return cfg;
    }

}
