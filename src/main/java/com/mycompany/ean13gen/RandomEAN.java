/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ean13gen;

import java.util.Hashtable;
import java.util.Random;

/**
 *
 * @author fabio
 */
public class RandomEAN {

    public static int EAN12 = 11;
    Hashtable<Long, Long> numbers;
    
    
    RandomEAN() {
        numbers = new Hashtable<>();
    }

    public long generateRandom() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        // first not 0 digit
        sb.append(random.nextInt(9) + 1);
        // rest of 11 digits
        for (int i = 0; i < EAN12; i++) {
            sb.append(random.nextInt(10));
        }
        return Long.parseLong(sb.toString());
    }

   
    public void exec() {
        long aux = 0;

        for (int i = 0; i < 1000; i++) {
            aux = this.generateRandom();
            if (numbers.contains(aux)) {
                System.out.println("************* Colisao");
            } else {
              numbers.put(aux,aux ); 
              System.out.println(aux);
            }

        }
    }

     public static void main(String[] args) {
        RandomEAN randomEAN = new RandomEAN();
        randomEAN.exec();
    }
    
    
}
